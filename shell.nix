{ withFile,
  pkgs ? import ./nixpkgs.nix {},
  stdenv ? pkgs.stdenv,
  texlive ? pkgs.texlive,
  sage ? pkgs.sage }:

let

  runLaTeX = pkgs.texFunctions.runLaTeX (
  {
  
    rootFile = withFile;
    extraFiles = [
      (sage.out + "/share/texmf/tex/latex/sagetex/")
      ./src/projects/crypto
      ./data
    ];
    packages = [];
    texPackages = {
      inherit (pkgs.texlive) collection-latexrecommended;
      };
    
  });

in

  runLaTeX.overrideAttrs (oldAttrs:
  rec {

    buildInputs = (oldAttrs.buildInputs or []) ++ [ sage ];
    semester = "su18";
    name = "psu-math220-${semester}-challenges";
    builder = ./run-latex-patched.sh;
    
  })
