FROM nixos/nix

RUN nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkg
RUN nix-channel --update

ADD src/ /opt/src
ADD shell.nix /opt/shell.nix
ADD nixpkgs.nix /opt/nixpkgs.nix

WORKDIR /opt
RUN nix-shell --arg withFile src/master.tex ./shell.nix
