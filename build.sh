#!/usr/bin/env sh

pdflatex -interaction=batchmode -enable-write18 master-*.tex

mkdir -p ${out}/pdf
mv master-*.pdf ${out}/pdf
chown 1000 ${out}/pdf/master-*.tex
