\documentclass{article}[11pt]

\usepackage{sagetex}
\usepackage[total={6in, 8in}]{geometry}
\usepackage{hyperref}
\usepackage{amsmath}


\begin{document}

\title{\textsc{math 220: Matrices} \\ \textbf{Projects}}

\maketitle

\section{The Hill cipher: how to break it?}
\vspace{-0.5em}\hspace{2em}\textsc{student(s) working on this: Matthew, Lin, Saif, Xiao} \\

\begin{sagesilent}
  import hill as crypto
  from itertools import chain

  def wrap_every(s, n):
     return '\n'.join([s[l:l+n] for l in range(0, len(s), n)])   
\end{sagesilent}
The Hill cipher is a linear cipher that is seldom used by itself but is, in some way, heavily used in actual applications (see, for a famous example, the \href{https://en.wikipedia.org/wiki/Advanced_Encryption_Standard}{AES specifications}). The following exaplanations apply to both versions of the problems specified in the next two subsections.

The encoding we shall use establishes a dictionary between letters of the alphabet together with three additional special characters (space, period and comma) and elements of $\mathbf{F}_{29}$, the finite field with $29$ elements where arithmetic is done modulo $29$. Here is the dictionary:
\begin{eqnarray*}
  &\sage{crypto.dictionary[0:10]}\\
  &\sage{crypto.dictionary[10:20]}\\
  &\sage{crypto.dictionary[20:]}
\end{eqnarray*}

Any $n\times n$ matrix $K$ that is invertible in $\mathbf{F}_{29}$ is a valid choice for a key. Encryption and decryption are done by multiplying length $n$-words (encoded using the above dictionary) by the chosen key matrix $K$ or its inverse $K^{-1}$ in $\mathbf{F}_{29}$.

The reason why the Hill cipher is seldom used by itself lies in the fact it is extremely weak to known plain-text attacks. Understanding that fact, in the two easiest modes of operation, namely ECB and CBC, is the objective of the projects in this section.

\subsection{Breaking the Hill cipher in ECB mode}

The ECB (\emph{electronic codebook}) mode of operation for the Hill cipher is the most natural way to use the cipher: let $s$ be a plaintext string we are trying to encrypt using a key $K$, say

\begin{sagesilent}
  s = 'attack at dawn'
  words = MatrixSpace(GF(29), 3, 1)
  n = 3
  K = crypto.random_inv_matrix(GF(29), 3)
\end{sagesilent}
\begin{eqnarray*}
  &s = \sage{s}, \quad K = \sage{K}
\end{eqnarray*}
To find its ciphertext counterpart, we first pad it to a length that is a multiple of $n = \sage{n}$ by adding spaces at the end. Then we encode the resulting string with the dictionary to get
\begin{sagesilent}
  e = crypto.encode(crypto.pad(s, n))
\end{sagesilent}
$$ \sage{e}. $$
We break the encoded plaintext down into words of length $\sage{n}$ and assemble each of those into a matrix to get:
\begin{sagesilent}
  ws = [words(e[w:w+n]) for w in range(0, len(s), n)]
\end{sagesilent}
$$ \sage{ws} $$
Then multiplying each word by the key matrix in $\mathbf{F}_{29}$ gives
\begin{sagesilent}
  cws = list(map(lambda w: K*w, ws))
\end{sagesilent}
$$ \sage{cws} $$
Finally, concatenating back and decoding yields the cipher text:
$$ \sage{''.join(map(crypto.decode, list(chain(*cws))))} $$

\subsubsection{Questions}

\begin{sagesilent}
  import secret
  p = secret.plaintext
  n_p = 5
  K_p = crypto.random_inv_matrix(GF(29), 5)
  cipher_p = crypto.hill_encrypt(p, K_p)
  padded_p = crypto.pad(p, n_p)
\end{sagesilent}

Here are the questions you \emph{have} to answer:
\begin{itemize}
\item[1.] Assume we managed to capture the following cipher text by listening in on a cable somewhere:
  $$\sage{wrap_every(cipher_p, 37)}$$
  Our super amazing spies also managed to capture the few following length $\sage{n_p}$ words from the plaintext (in no particular order):
  \begin{sagesilent}
    import numpy as np
    ws_i = np.random.choice(range(0, len(padded_p)/n_p), size=(5,), replace=false)
    plain_w = [padded_p[i*n_p:(i+1)*n_p] for i in ws_i]
    show_plain_w = "\n".join(plain_w)
  \end{sagesilent}
  $$ \sage{show_plain_w} $$
  Your task is to recover the entirety of the secret message \emph{and} the key that was used to encrypt it by writing a program that will recover the latter without the use of brute force.
\item[2.] Now assume, in the problem above, that you were \textbf{not} given a total of $\sage{n_p}$ words from the plaintext, but slightly less, say $\sage{n_p-1}$. Write a program (and show it working) that would be able to solve the problem (under reasonable assumptions) in at most $29^{\sage{n_p}}$ tries. You can run it on the example above or your own.
\end{itemize}

\subsection{Breaking the Hill cipher in CBC mode}

The CBC (\emph{cipher block chaining}) mode of operation for the Hill cipher is a simple way to render it a little more resilient against a variety of attacks. For more details on how it is implemented for this project, see the crypto.hill\_*crypt\_cbc functions.

Its single most important properties is the following fact. Consider the example of the plaintext $s$ above
$$ s = \sage{s} $$
Encrypted with the same key matrix as in that example, we'd get the string (compare it with the string obtained there in the ECB mode)
$$ \sage{crypto.hill_encrypt_cbc(s, K)} $$
But now, assume we change a single letter in the plaintext, say to
\begin{sagesilent}
  ns = 'b' + s[1:]
\end{sagesilent}
$$ ns = \sage{ns} $$
Then the cipher text changes \emph{completely} to
$$ \sage{crypto.hill_encrypt_cbc(ns, K)} $$
This is to be expected since the point of CBC is to linearly propagate the cipher.

\subsubsection{Questions}
Here is the question you \emph{have} to answer on the CBC mode of operation:
\begin{itemize}
\item[3.] Would the attack(s) you developped in steps 1 and 2 above work if CBC is used? Explain. If you answered ``no'', what additional information would you need in order to make them work?
\end{itemize}

\section{Markov chains: steady state distributions}
\vspace{-0.5em}\hspace{2em}\textsc{student(s) working on this: Devansh, Zhihui} \\

A Markov chain is a kind of stochastic process $X_n$ ($n = 0, \ldots, +\infty$) that satisfies that
\begin{equation}\label{markov}
  P(X_{n+1} = x_0 \,|\, X_n, \ldots, X_0) = P(X_{n+1} = x_0 \,|\, X_n)
\end{equation}
In other words, the only information we need in order to estimate the most ``likely value'' one time step away is what its current value is right now. We further say that such a process is \emph{stationary} if we have
\begin{equation}
  P(X_{n+1} = x_0 | X_n ) = P(X_{n} = x_0 \,|\, X_{n-1})
\end{equation}

The particular process we will be studying is the following. Tweets of a famous personality were collected for the period of \textsc{2018-06-24} to \textsc{2017-08-07} and passed through a tone recognition SVM (the \emph{IBM Watson Tone Analyzer}). This yielded a daily timeseries of analyzed moods (of \emph{very} questionable accuracy), a sample of which is in the following:
\begin{sagesilent}
  import csv
  moods = [[d, mood] for d, mood in csv.reader(open('markov.csv'))]
  sample_s = np.random.randint(0, len(moods)-4)
\end{sagesilent}
\begin{eqnarray*}
  & \sage{moods[sample_s]}\\
  & \sage{moods[sample_s+1]}\\
  & \sage{moods[sample_s+2]}\\
  & \sage{moods[sample_s+3]}\\
  & \sage{moods[sample_s+4]}
\end{eqnarray*}
In particular, our process has a finite number of different possible states, namely (as per the \href{https://www.ibm.com/watson/developercloud/tone-analyzer/api/v3/python.html?python#tone}{API Documentation}): \emph{anger, disgust, fear, joy, sadness, analytical, confident and tentative}. If we assume that this process is stationary and Markovian, there must exists a $9\times 9$ transition matrix $A$ encoding the probabilities in Eq.\ (\ref{markov}) by
$$ a_{ij} = P(X_n = s_i | X_{n-1} = s_j) $$
where $s_i, s_j$ are any two of the moods defined above and where $n$ is any point in time.

\subsubsection{Questions}

Here are the questions you \emph{have} to answer:
\begin{itemize}
\item[1.] Explain your understanding of a Markov chain. Explain how a Markov process relates to a random walk in a graph.
\item[2.] Find a reasonable way to estimate the transition matrix $A$ and compute it. The full dataset to be used can be found at \href{https://storage.googleapis.com/matrices-su18/markov.csv}{https://storage.googleapis.com/matrices-su18/markov.csv}.
\item[3.] Compute the daily, weekly, monthly and yearly transition probabilities. How do they compare?
\item[4.] Find the steady-state distribution vector $\pi$ in $\mathbf{R}^9$ such that $A \pi = \pi$, where $A$ is the matrix you computed in 2. Interpret your result.
\end{itemize}


\section{Graphs: the dynamics of New York city bike trips}
\vspace{-0.5em}\hspace{2em}\textsc{student(s) working on this: Yi} \\

From any graph with vertex set $V$ and edge set $E$ one defines an adjacency matrix $A$ as follows. We let, for any two $i, j$ in $V$, the entry $a_{ij}$ of $A$ be $1$ if $(i, j)$ is an edge in $E$ and $0$ otherwise. The adjacency matrix has plenty of interesting properties that encode the topology of the graph. Further, if the graph has decorated edges (say, with weights $w_{ij}$), then we can more generally let $a_{ij} = w_{ij}$ instead of $1$.

In this project we will study the long term dynamics of bike trips in New York City. Using one of the amazing public datasets available on Google BigQuery, namely the \href{https://cloud.google.com/bigquery/public-data/nyc-citi-bike}{New York City Bike Trips} dataset, we extracted the daily number of trips between any two stations available on the bike share network for the $4$ years leading up to $2016$. The full dataset can be found at \href{https://storage.googleapis.com/matrices-su18/biketrips.csv}{https://storage.googleapis.com/matrices-su18/biketrips.csv}.
\begin{sagesilent}
  trips_reader = csv.reader(open('biketrips.csv'))
  trips = [next(trips_reader) for i in range(100)]
  sample_t = np.random.randint(0, len(trips)-5)
\end{sagesilent}
It's a csv whose columns are, in order, defined by the list $\sage{trips[0]}$. Here is a sample of it:
\begin{eqnarray*}
  & \sage{trips[sample_t]}\\
  & \sage{trips[sample_t+1]}\\
  & \sage{trips[sample_t+2]}\\
  & \sage{trips[sample_t+3]}\\
  & \sage{trips[sample_t+4]}
\end{eqnarray*}
The goal of this project is to construct a graph whose vertices are stations on the ride sharing service and whose edges (and their weights) represent the average amount of daily traffic flowing from one station to the next.

\subsubsection{Questions}
Here are the questions you \emph{have} to answer:
\begin{itemize}
\item[1.] Find a way to define a graph whose edges are weighted by weights $w_{ij}$ such that the larger $w_{ij}$ is, the more daily trips there are  (on average) between stations $i$ and $j$. Construct its adjacency matrix.
\item[2.] Assuming each station has around $10$ bikes available at the beginning of the day, what is the expected distribution of the bikes by the end of the day? What about $7$, $30$, and $360$ days from now? Interpret your result.
\item[4.] Does the assumption of having $10$ bikes at each station matter? What would be different if we didn't assume that?
\item[5.] Assuming there are a total of around $8000$ bikes in circulation at any given time, what is the asymptotic (i.e.\ long term) expected distribution of those bikes per station in the network? Which station ends up with the most visits daily? Which stations ends up with the least?
\end{itemize}


\section{The Leslie population model: population dynamics}
\vspace{-0.5em}\hspace{2em}\textsc{student(s) working on this: Fahad} \\
The Leslie population model is a linear dynamical system for modelling the evolution in time of the population of a species that does not have predators. More specifically, assume
$$ X(t) = \left[\begin{matrix} x_1(t) \\ x_2(t) \\ \vdots \\ x_n(t) \end{matrix}\right] $$
is a vector whose coordinate $x_i(t)$ is the census of our population at a given age range $[a_i, a_{i+1})$. Then the model is encoded in a $n \times n$ matrix $A$ so that its time evolution is simply given by
  $$ X(t+1) = A X(t). $$
  Further, in the Leslie population model, the matrix $A$ is constrained to be of the form:
  $$ A = \left[\begin{matrix} r_1 & r_2 & \cdots & r_{n-1} & r_n \\ s_1 & 0 & \cdots & 0 & 0 \\ 0 & s_2 & \cdots & 0 & 0 \\ \vdots & \vdots & \ddots & \vdots & \vdots \\ 0 & 0 & \cdots & s_{n-1} & 0\end{matrix}\right] $$
  Google has \emph{amazing} public BigQuery datasets for studying population models. For this project, we will be using the \href{https://cloud.google.com/bigquery/public-data/international-census}{United States Census Bureau International Data} and the \href{https://cloud.google.com/bigquery/public-data/us-census}{United States Census Data}. 
  \subsubsection{Questions}
  Here are the questions you \emph{have} to answer:
  \begin{itemize}
  \item[1.] Interpret the coefficients $r_1, \ldots, r_n$ and $s_1, \ldots, s_{n-1}$ in sociological terms. What do they represent? How do they impact the time evolution of the system?
  \item[2.] Find a way to extract the data you need in order to fit and define a Leslie matrix $A$ to model the population statistics of the United States (or some other population of your choice) from the datasets mentionned above (or another which might be easier to you).
  \item[3.] Given the model matrix $A$ you found in 3, construct and interpret a model for (respectively) the $10$, $20$ and $50$ year evolution of the system. Interpret your result.
  \item[4.] How does the model evolve asymptotically? Why?
  \item[5.] Compare the asymptotic growth rate of your model with reality. How well does it fit? Can it be explained?
  \end{itemize}

\end{document}
