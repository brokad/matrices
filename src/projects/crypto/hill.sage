from itertools import chain

k = GF(29)  # ground field -- do not change!
aToZ = range(ord('a'), ord('z') + 1)
aToZ_withSpecialCharacters = chain(aToZ, [ord(' '), ord(','), ord('.')])
dictionary = [(chr(c), k(v)) for v, c in enumerate(aToZ_withSpecialCharacters)]


def fromEncoded(n):
    
    for c, v in dictionary:
        if v == n:
            return c
        
    raise ValueError("The value " + str(n) + " is invalid.")


def fromChar(c):
    
    for ch, v in dictionary:
        if c == ch:
            return v
        
    raise ValueError(str(c) + " is not a valid character.")


def encode(s):
    '''
    encode a string `s` using the dictionary for the hill cipher
    '''
    return list(map(fromChar, s.lower()))


def decode(a):
    '''
    decode an array `a` of encoded characters to usual characters
    '''
    return ''.join(list(map(fromEncoded, a)))


def _safeGetKeyDimensions(key):

    try:

        n = len(key)
        m = len(key[0])

    except TypeError:

        try:

            n, m = key.dimensions()

        except AttributeError:

            try:

                import numpy as np
                _key = np.array(key)
                n, m = _key.shape

            except ValueError as e:

                raise ValueError("Unable to determine the dimensions of `key`.")

    except IndexError:

        raise ValueError("The given key `key` has invalid dimensions.")

    assert(n == m)  # matrix must be square!

    return n


def pad(s, n):
    '''
    pads a string `s` to a length that is a multiple of `n` using spaces
    '''
    
    rem = len(s) % n

    if rem != 0:

        padded = s + " "*(n-rem)

    else:

        padded = s

    return padded


def _safePrepareKey(key):
    '''
    prepares a given key to be used with Sage's matrix algebra over finite fields.
    returns:
       - W: the matrix space over words
       - K: the matrix space over keys
       - _key: the key casted as a Sage matrix over k
       - n: the dimension of _key
    '''
    n = _safeGetKeyDimensions(key)

    W = MatrixSpace(k, n, 1)  # matrix space for word matrices
    K = MatrixSpace(k, n, n)  # matrix space for key matrices

    try:

        _key = K(key)

    except ValueError:

        raise("Was unable to use the provided `key` with Sage's matrices over finite fields.")

    assert(_key.is_invertible())  # non-invertible keys do not lead to symmetric ciphers!
    
    return W, K, _key, n


def hill_encrypt(s, key):
    '''
    encrypt a string `s` using the Hill cipher with key (n x n) matrix `key` in the following way:
        - pads the end of the string with spaces if necessary so that the length is divisible by the # of columns of `key`
        - encodes the string `s` with encode(s),
        - breaks the resulting string down into words of length = # columns of `key`
        - multiplies each word by `key`
        - decodes the resulting cipher text and concatenate.
    '''

    W, K, _key, n = _safePrepareKey(key)

    plain_text = pad(s, n)
    
    encoded_plain_text = encode(plain_text)

    encoded_cipher_words = [_key * W(encoded_plain_text[w:w+n])
                            for w in range(0, len(encoded_plain_text), n)]

    encoded_cipher_text = list(map(lambda c: c[0], chain(*encoded_cipher_words)))

    cipher_text = decode(encoded_cipher_text)

    return cipher_text


def hill_decrypt(s, decryption_key):
    '''
    decrypts a hill cipher string `s` encrypted with the hill cipher described in hill_encrypt
    '''
    
    n = _safeGetKeyDimensions(decryption_key)
    
    assert(len(s) % n == 0)  # `s` must be a valid cipher text. In particular, it should be padded already!
    
    return hill_encrypt(s, decryption_key)


def hill_encrypt_cbc(s, key):
    '''
    encrypts a string `s` using the Hill cipher (in CBC mode, with IV=0) with key (n x n) matrix `key`
    '''

    W, K, _key, n = _safePrepareKey(key)

    plain_text = pad(s, n)
    
    encoded_plain_text = encode(plain_text)

    encoded_cipher_words = [_key * W(encoded_plain_text[0:n])]  # first cipher word

    for w in range(n, len(encoded_plain_text), n):

        plain_text_word = W(encoded_plain_text[w:w+n])
        prev_cipher_word = encoded_cipher_words[w/n - 1]
        chained_cipher_word = _key * (plain_text_word + prev_cipher_word)
        encoded_cipher_words += [chained_cipher_word]

    encoded_cipher_text = list(map(lambda c: c[0],
                                   chain(*encoded_cipher_words)))

    cipher_text = decode(encoded_cipher_text)

    return cipher_text


def hill_decrypt_cbc(s, decryption_key):
    '''
    decrypts a cipher text `s` that was encrypted by hill_encrypt_cbc
    '''

    W, K, _key, n = _safePrepareKey(key)

    assert(len(s) % n == 0)  # any valid cipher text needs to have length divisible by n
    
    cipher_text = s

    encoded_cipher_text = encode(cipher_text)

    encoded_plain_words = []
    for w in reversed(range(n, len(encoded_cipher_text), n)):

        chained_cipher_text_word = W(encoded_cipher_text[w:w+n])
        prev_cipher_word = W(encoded_cipher_text[w-n:w])
        encoded_plain_word = _key * chained_cipher_text_word - prev_cipher_word
        encoded_plain_words += [encoded_plain_word]

    encoded_plain_words += [_key * W(encoded_cipher_text[0:n])]

    encoded_plain_text = list(map(lambda c: c[0],
                                  chain(*reversed(encoded_plain_words))))

    plain_text = decode(encoded_plain_text)

    return plain_text

def random_inv_matrix(k, n):
     K = random_matrix(k, n, n)
     while not K.is_invertible():
        K = random_matrix(k, n, n)
     return K
